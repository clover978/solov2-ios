//
//  ViewController.h
//  squeenzecnn
//
//  Created by dang on 2017/7/28.
//  Copyright © 2017年 dang. All rights reserved.
//

// Cplusplus STL
#include <algorithm>
#include <functional>
#include <vector>

// ncnn
#include "solov2.h"

// iOS UI
#import "ViewController.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UIKit/UIImage.h>


@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView* imageview_ori;
@property (strong, nonatomic) IBOutlet UIImageView* imageview_res;

//@property (strong, nonatomic) IBOutlet UIButton* bn_select;
//@property (strong, nonatomic) IBOutlet UIButton* bn_predict;
- (IBAction)select:(id)sender;
- (IBAction)predict:(id)sender;

@property SOLOV2* solov2;

@end
