//
//  ViewController.mm
//  squeenzecnn
//
//  Created by dang on 2017/7/28.
//  Copyright © 2017年 dang. All rights reserved.
//

#include "ViewController.h"

#include <opencv2/imgcodecs/ios.h>


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.solov2 = new SOLOV2();
    self.imageview_ori.image = [UIImage imageNamed:@"ori.jpg"];
    self.imageview_res.image = [UIImage imageNamed:@"res.jpg"];
}


#pragma mark picture selector
- (IBAction)select:(id)sender {
    UIImagePickerController* picVC = [UIImagePickerController new];
    picVC.delegate = self;
    picVC.allowsEditing = NO;
    [self presentViewController:picVC animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController* )picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    self.imageview_ori.image = [UIImage imageWithCGImage:[image CGImage] scale:[image scale] orientation:UIImageOrientationUp];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark model inference
- (IBAction)predict:(id)sender {
    UIImage* image = self.imageview_ori.image;
    
    // get input image.
    int img_w = image.size.width;
    int img_h = image.size.height;
    cv::Mat rgba(img_h, img_w, CV_8UC4);
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGContextRef contextRef = CGBitmapContextCreate(rgba.data, img_w, img_h, 8, rgba.step[0], colorSpace, kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault);
    CGContextDrawImage(contextRef, CGRectMake(0, 0, img_w, img_h), image.CGImage);
    // CGColorSpaceRelease(colorSpace);            // we are not responsable to release.
    CGContextRelease(contextRef);
    
    std::vector<Instance> masks;
    cv::Mat res;
    self.solov2->detect_solov2(rgba, masks);
    self.solov2->draw_objects(rgba, res, masks);    // return cv::Mat(RGB) result.
    self.imageview_res.image = [self drawMat:res];  // return UIImage* result.
    
    // delete self.solov2;
}


#pragma mark visualize callback
- (UIImage *) drawMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;

    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaNone;
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGBitmapByteOrder32Little | (
            cvMat.elemSize() == 3? kCGImageAlphaNone : kCGImageAlphaNoneSkipFirst
        );
    }

    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);

    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,
                                        cvMat.rows,
                                        8,
                                        8 * cvMat.elemSize(),
                                        cvMat.step[0],
                                        colorSpace,
                                        bitmapInfo,
                                        provider,
                                        NULL,
                                        false,
                                        kCGRenderingIntentDefault
                                        );

    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    return finalImage;
}

@end
