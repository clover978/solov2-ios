//
//  solov2.m
//  squeenzecnn
//
//  Created by clover on 2021/5/21.
//  Copyright © 2021 dang. All rights reserved.
//

#include "solov2.h"


// DEBUG helper function
UIImage* UIImageFromCVMat(cv::Mat cvMat) {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;

    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGBitmapByteOrder32Little | (
            cvMat.elemSize() == 3? kCGImageAlphaNone : kCGImageAlphaNoneSkipFirst
        );
    }

    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);

    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,
                                        cvMat.rows,
                                        8,
                                        8 * cvMat.elemSize(),
                                        cvMat.step[0],
                                        colorSpace,
                                        bitmapInfo,
                                        provider,
                                        NULL,
                                        false,
                                        kCGRenderingIntentDefault
                                        );

    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    // CFRelease((__bridge CFDataRef)data);
    return finalImage;
}

void print_ncnn_mat(const ncnn::Mat& m) {
    for (int q=0; q<m.c; q++)
    {
        const float* ptr = m.channel(q);
        for (int y=0; y<m.h; y++)
        {
            for (int x=0; x<m.w; x++)
            {
                printf("%f ", ptr[x]);
            }
            ptr += m.w;
            printf("\n");
        }
        printf("------------------------\n");
    }
}

void diff_ncnn_mat(const ncnn::Mat &a, const ncnn::Mat &b) {
    int count = 0;
    if(a.c == b.c && a.h == b.h && a.w == b.w){
        for (int q=0; q<a.c; q++)
        {
            const float* ptr1 = a.channel(q);
            const float* ptr2 = b.channel(q);
            for (int y=0; y<a.h; y++)
            {
                for (int x=0; x<a.w; x++)
                {
                    if(abs(ptr1[x]-ptr2[x]) > 1e-2)  count += 1;
                }
                ptr1 += a.w;
                ptr2 += b.w;
            }
        }
        std::cout << "diff elements: " << count << std::endl;
    }
    else{
        std::cout << "diff elements: " << "shape mismatch." << std::endl;
    }
}


// SOLOV2 constructor
SOLOV2::SOLOV2() {
    // TODO: add gpu compute
    solov2_model = new ncnn::Net();
    solov2_model->opt.num_threads = 8;
    solov2_model->opt.use_vulkan_compute = false;
    solov2_model->opt.use_fp16_arithmetic = true;
    
    
    NSString *paramPath = [ [NSBundle mainBundle] pathForResource:@"solov2_op" ofType:@"param"];
    NSString *binPath = [ [NSBundle mainBundle] pathForResource:@"solov2_op" ofType:@"bin"];
    int r0 = solov2_model->load_param([paramPath UTF8String]);
    int r1 = solov2_model->load_model([binPath UTF8String]);
    if (r0 == 0 && r1 == 0){
        printf("load model success!\n===================\n");
    }
    else{
        printf("load model failed!\n===================\n");
    }
}


// SOLOV2 deconstructor
SOLOV2::~SOLOV2() {
    solov2_model->clear();
    delete solov2_model;
}


// SOLOV2 methods
void SOLOV2::detect_solov2(const cv::Mat &rgba, std::vector<Instance> &objects) {

    const float cate_thresh = 0.4f;
    const float confidence_thresh = 0.15f;
    const float nms_threshold = 0.3f;
    const int keep_top_k = 200;
    const float mean_vals[3] = {123.68f, 116.78f, 103.94f};
    const float norm_vals[3] = {1.0 / 58.40f, 1.0 / 57.12f, 1.0 / 57.38f};
    
    int img_w = rgba.cols;
    int img_h = rgba.rows;
    cv::Mat rgba_pad = rgba.clone();
    // TODO: add unpad
    // float ratio = input_w * 1.0 / input_h;
    // int pad_w = 0, pad_h = 0;
    // if (img_w / img_h < ratio){
    //    pad_w = img_h * ratio - img_w;
    // }
    // else{
    //     pad_h = img_w / ratio - img_h;
    // }
    // cv::copyMakeBorder(rgba, rgba_pad, 0, 0, pad_h, pad_w, cv::BORDER_CONSTANT);         // view rgba when debug: UIImageFromCVMat(rgba_pad)
    // ################
    
    ncnn::Mat in = ncnn::Mat::from_pixels_resize(rgba_pad.data, ncnn::Mat::PIXEL_RGBA2RGB, img_w, img_h, input_w, input_h);
    in.substract_mean_normalize(mean_vals, norm_vals);
    
    
    // coord conv
    size_t elemsize = sizeof(float);
    ncnn::Mat x_p3;
    ncnn::Mat x_p4;
    ncnn::Mat x_p5;

    int pw = int(input_w / 8);
    int ph = int(input_h / 8);
    x_p3.create(pw, ph, 2, elemsize);
    float step_h = 2.f / (ph - 1);
    float step_w = 2.f / (pw - 1);
    for (int h = 0; h < ph; h++) {
        for (int w = 0; w < pw; w++) {
            x_p3.channel(0)[h * pw + w] = -1.f + step_w * (float) w;
            x_p3.channel(1)[h * pw + w] = -1.f + step_h * (float) h;
        }
    }

    pw = int(input_w / 16);
    ph = int(input_h / 16);
    x_p4.create(pw, ph, 2, elemsize);
    step_h = 2.f / (ph - 1);
    step_w = 2.f / (pw - 1);
    for (int h = 0; h < ph; h++) {
        for (int w = 0; w < pw; w++) {
            x_p4.channel(0)[h * pw + w] = -1.f + step_w * (float) w;
            x_p4.channel(1)[h * pw + w] = -1.f + step_h * (float) h;
        }
    }

    pw = int(input_w / 32);
    ph = int(input_h / 32);
    x_p5.create(pw, ph, 2, elemsize);
    step_h = 2.f / (ph - 1);
    step_w = 2.f / (pw - 1);
    for (int h = 0; h < ph; h++) {
        for (int w = 0; w < pw; w++) {
            x_p5.channel(0)[h * pw + w] = -1.f + step_w * (float) w;
            x_p5.channel(1)[h * pw + w] = -1.f + step_h * (float) h;
        }
    }

    ncnn::Extractor ex = solov2_model->create_extractor();
    ex.set_light_mode(true);        // default true. memery overload when setting to false.
    ncnn::Mat feature_pred, cate_pred1, cate_pred2, cate_pred3, cate_pred4, cate_pred5, kernel_pred1, kernel_pred2, kernel_pred3, kernel_pred4, kernel_pred5;
    ex.input("input", in);
    ex.input("p3_input", x_p3);
    ex.input("p4_input", x_p4);
    ex.input("p5_input", x_p5);

//    ex.extract("feature_pred", feature_pred);
//    ex.extract("kernel_pred1", kernel_pred1);
//    ex.extract("kernel_pred2", kernel_pred2);
//    ex.extract("kernel_pred3", kernel_pred3);
//    ex.extract("kernel_pred4", kernel_pred4);
//    ex.extract("kernel_pred5", kernel_pred5);
//    ex.extract("cate_pred1", cate_pred1);
//    ex.extract("cate_pred2", cate_pred2);
//    ex.extract("cate_pred3", cate_pred3);
//    ex.extract("cate_pred4", cate_pred4);
//    ex.extract("cate_pred5", cate_pred5);

    ex.extract("feature_pred", feature_pred);
    ex.extract("kernel_pred5", kernel_pred5); ex.extract("cate_pred5", cate_pred5);
    ex.extract("kernel_pred4", kernel_pred4); ex.extract("cate_pred4", cate_pred4);
    ex.extract("kernel_pred3", kernel_pred3); ex.extract("cate_pred3", cate_pred3);
    ex.extract("kernel_pred2", kernel_pred2); ex.extract("cate_pred2", cate_pred2);
    ex.extract("kernel_pred1", kernel_pred1); ex.extract("cate_pred1", cate_pred1);
  
//    static ncnn::Mat _in;
//    static ncnn::Mat _kernel_pred1;
//    static ncnn::Mat _kernel_pred2;
//    static ncnn::Mat _kernel_pred3;
//    static ncnn::Mat _kernel_pred4;
//    static ncnn::Mat _kernel_pred5;
//    static ncnn::Mat _cate_pred1;
//    static ncnn::Mat _cate_pred2;
//    static ncnn::Mat _cate_pred3;
//    static ncnn::Mat _cate_pred4;
//    static ncnn::Mat _cate_pred5;
//    static ncnn::Mat _feature_pred;
//    diff_ncnn_mat(in, _in);
//    diff_ncnn_mat(kernel_pred1, _kernel_pred1);
//    diff_ncnn_mat(kernel_pred2, _kernel_pred2);
//    diff_ncnn_mat(kernel_pred3, _kernel_pred3);
//    diff_ncnn_mat(kernel_pred4, _kernel_pred4);
//    diff_ncnn_mat(kernel_pred5, _kernel_pred5);
//    diff_ncnn_mat(cate_pred1, _cate_pred1);
//    diff_ncnn_mat(cate_pred2, _cate_pred2);
//    diff_ncnn_mat(cate_pred3, _cate_pred3);
//    diff_ncnn_mat(cate_pred4, _cate_pred4);
//    diff_ncnn_mat(cate_pred5, _cate_pred5);
//    diff_ncnn_mat(feature_pred, _feature_pred);
//    _in = in.clone();
//    _kernel_pred1 = kernel_pred1.clone();
//    _kernel_pred2 = kernel_pred2.clone();
//    _kernel_pred3 = kernel_pred3.clone();
//    _kernel_pred4 = kernel_pred4.clone();
//    _kernel_pred5 = kernel_pred5.clone();
//    _cate_pred1 = cate_pred1.clone();
//    _cate_pred2 = cate_pred2.clone();
//    _cate_pred3 = cate_pred3.clone();
//    _cate_pred4 = cate_pred4.clone();
//    _cate_pred5 = cate_pred5.clone();
//    _feature_pred = feature_pred.clone();

    
    //ins decode
    ncnn::Option opt;
    opt.num_threads = 8;
    opt.use_fp16_storage = false;
    opt.use_packing_layout = false;
    ncnn::Mat ins_pred1, ins_pred2, ins_pred3, ins_pred4, ins_pred5;
    int c_in = feature_pred.c;
    
    ins_decode(kernel_pred1, feature_pred, &ins_pred1, c_in, 40 * 40,opt);
    ins_decode(kernel_pred2, feature_pred, &ins_pred2, c_in, 36 * 36,opt);
    ins_decode(kernel_pred3, feature_pred, &ins_pred3, c_in, 24 * 24,opt);
    ins_decode(kernel_pred4, feature_pred, &ins_pred4, c_in, 16 * 16,opt);
    ins_decode(kernel_pred5, feature_pred, &ins_pred5, c_in, 12 * 12,opt);

    
    // ins pred
    int num_class = cate_pred1.c;
    std::vector <std::vector<Instance>> class_candidates;
    class_candidates.resize(num_class);
    
    generate_res(cate_pred1,ins_pred1,class_candidates,cate_thresh,confidence_thresh,img_w,img_h,num_class,8.f);
    generate_res(cate_pred2,ins_pred2,class_candidates,cate_thresh,confidence_thresh,img_w,img_h,num_class,8.f);
    generate_res(cate_pred3,ins_pred3,class_candidates,cate_thresh,confidence_thresh,img_w,img_h,num_class,16.f);
    generate_res(cate_pred4,ins_pred4,class_candidates,cate_thresh,confidence_thresh,img_w,img_h,num_class,32.f);
    generate_res(cate_pred5,ins_pred5,class_candidates,cate_thresh,confidence_thresh,img_w,img_h,num_class,32.f);

    
    // nms
    for (int i = 0; i < (int) class_candidates.size(); i++) {
        std::vector <Instance> &candidates = class_candidates[i];

        qsort_descent_inplace(candidates);

        std::vector<int> picked;
        nms_sorted_segs(candidates, picked, nms_threshold,img_w,img_h);

        for (int j = 0; j < (int) picked.size(); j++) {
            int z = picked[j];
            objects.push_back(candidates[z]);
        }
    }

    qsort_descent_inplace(objects);

    // keep_top_k
    if (keep_top_k < (int) objects.size()) {
        objects.resize(keep_top_k);
    }
}


void SOLOV2::ins_decode(const ncnn::Mat &kernel_pred, const ncnn::Mat &feature_pred, ncnn::Mat *ins_pred, int c_in, int c_out, ncnn::Option &opt) {

    ncnn::Layer *op = ncnn::create_layer("Convolution");
    ncnn::ParamDict pd;
    pd.set(0, c_out);
    pd.set(1, 1);
    pd.set(6, c_in * c_out);
    op->load_param(pd);
    ncnn::Mat weights[1];
    weights[0].create(c_in * c_out);
    float *kernel_pred_data = (float *) kernel_pred.data;

    for (int i = 0; i < c_in * c_out; i++) {
        weights[0][i] = kernel_pred_data[i];
    }

    op->load_model(ncnn::ModelBinFromMatArray(weights));
    op->create_pipeline(opt);
    ncnn::Mat temp_ins;
    op->forward(feature_pred, temp_ins, opt);
    *ins_pred = temp_ins;
    op->destroy_pipeline(opt);
    delete op;
}


void SOLOV2::generate_res(ncnn::Mat &cate_pred, ncnn::Mat &ins_pred, std::vector <std::vector<Instance>> &objects,
                          float cate_thresh, float conf_thresh, int img_w, int img_h, int num_class, float stride) {
    
    int w = cate_pred.w;
    int h = cate_pred.h;
    int w_ins = ins_pred.w;
    int h_ins = ins_pred.h;
    for (int q = 0; q < num_class; q++) {
        const float *cate_ptr = cate_pred.channel(q);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int index = i * w + j;
                float cate_socre = cate_ptr[index];
                if (cate_socre < cate_thresh) {
                    continue;
                }
                const float *ins_ptr = ins_pred.channel(index);
                cv::Mat mask(h_ins, w_ins, CV_32FC1);
                float sum_mask = 0.f;
                int count_mask = 0;
                {
                    mask = cv::Scalar(0.f);
                    float *mp = (float *) mask.data;
                    for (int m = 0; m < w_ins * h_ins; m++) {
                        float mask_score = sigmoid(ins_ptr[m]);

                        if (mask_score > 0.5) {
                            mp[m] += mask_score;
                            sum_mask += mask_score;
                            count_mask++;
                        }
                    }
                }
                if (count_mask < stride) {
                    continue;
                }
                float mask_score = sum_mask / (float(count_mask) + 1e-6);

                float socre = mask_score * cate_socre;

                if (socre < conf_thresh) {
                    continue;
                }

                cv::Mat mask2;
                cv::resize(mask, mask2, cv::Size(img_w, img_h));
                Instance obj;
                obj.mask = cv::Mat(img_h, img_w, CV_8UC1);
                float sum_mask_y = 0.f;
                float sum_mask_x = 0.f;
                int area = 0;
                {
                    // obj.mask = cv::Scalar(0);
                    for (int y = 0; y < img_h; y++) {
                        const float *mp2 = mask2.ptr<const float>(y);
                        uchar *bmp = obj.mask.ptr<uchar>(y);
                        for (int x = 0; x < img_w; x++) {

                            if (mp2[x] > 0.5f) {
                                bmp[x] = 255;
                                sum_mask_y += (float) y;
                                sum_mask_x += (float) x;
                                area++;

                            } else bmp[x] = 0;
                        }
                    }
                }
                obj.cx = int(sum_mask_x / area);
                obj.cy = int(sum_mask_y / area);
                obj.label = q + 1;
                obj.prob = socre;
                objects[q].push_back(obj);
            }
        }
    }
}


void SOLOV2::nms_sorted_segs(const std::vector <Instance> &objects, std::vector<int> &picked, float nms_threshold, int img_w, int img_h) {

    picked.clear();

    const int n = (int)objects.size();

    std::vector<float> areas(n);
    for (int i = 0; i < n; i++) {
        areas[i] = area(objects[i], img_w, img_h);
    }

    for (int i = 0; i < n; i++) {
        const Instance &a = objects[i];

        int keep = 1;
        for (int j = 0; j < (int) picked.size(); j++) {
            const Instance &b = objects[picked[j]];

            // intersection over union
            float inter_area = intersection_area(a, b, img_w, img_h);
            float union_area = areas[i] + areas[picked[j]] - inter_area;
            if (inter_area / union_area > nms_threshold)
                keep = 0;
        }

        if (keep)
            picked.push_back(i);
    }
}


void SOLOV2::draw_objects(const cv::Mat &rgba, cv::Mat &res, const std::vector <Instance> &objects) {
    static const char *class_names[] = {"background",
                                        "person", "bicycle", "car", "motorcycle", "airplane", "bus",
                                        "train", "truck", "boat", "traffic light", "fire hydrant",
                                        "stop sign", "parking meter", "bench", "bird", "cat", "dog",
                                        "horse", "sheep", "cow", "elephant", "bear", "zebra", "giraffe",
                                        "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee",
                                        "skis", "snowboard", "sports ball", "kite", "baseball bat",
                                        "baseball glove", "skateboard", "surfboard", "tennis racket",
                                        "bottle", "wine glass", "cup", "fork", "knife", "spoon", "bowl",
                                        "banana", "apple", "sandwich", "orange", "broccoli", "carrot",
                                        "hot dog", "pizza", "donut", "cake", "chair", "couch",
                                        "potted plant", "bed", "dining table", "toilet", "tv", "laptop",
                                        "mouse", "remote", "keyboard", "cell phone", "microwave", "oven",
                                        "toaster", "sink", "refrigerator", "book", "clock", "vase",
                                        "scissors", "teddy bear", "hair drier", "toothbrush"
    };

    static const unsigned char colors[81][3] = {
            {56,  0,   255},
            {226, 255, 0},
            {0,   94,  255},
            {0,   37,  255},
            {0,   255, 94},
            {255, 226, 0},
            {0,   18,  255},
            {255, 151, 0},
            {170, 0,   255},
            {0,   255, 56},
            {255, 0,   75},
            {0,   75,  255},
            {0,   255, 169},
            {255, 0,   207},
            {75,  255, 0},
            {207, 0,   255},
            {37,  0,   255},
            {0,   207, 255},
            {94,  0,   255},
            {0,   255, 113},
            {255, 18,  0},
            {255, 0,   56},
            {18,  0,   255},
            {0,   255, 226},
            {170, 255, 0},
            {255, 0,   245},
            {151, 255, 0},
            {132, 255, 0},
            {75,  0,   255},
            {151, 0,   255},
            {0,   151, 255},
            {132, 0,   255},
            {0,   255, 245},
            {255, 132, 0},
            {226, 0,   255},
            {255, 37,  0},
            {207, 255, 0},
            {0,   255, 207},
            {94,  255, 0},
            {0,   226, 255},
            {56,  255, 0},
            {255, 94,  0},
            {255, 113, 0},
            {0,   132, 255},
            {255, 0,   132},
            {255, 170, 0},
            {255, 0,   188},
            {113, 255, 0},
            {245, 0,   255},
            {113, 0,   255},
            {255, 188, 0},
            {0,   113, 255},
            {255, 0,   0},
            {0,   56,  255},
            {255, 0,   113},
            {0,   255, 188},
            {255, 0,   94},
            {255, 0,   18},
            {18,  255, 0},
            {0,   255, 132},
            {0,   188, 255},
            {0,   245, 255},
            {0,   169, 255},
            {37,  255, 0},
            {255, 0,   151},
            {188, 0,   255},
            {0,   255, 37},
            {0,   255, 0},
            {255, 0,   170},
            {255, 0,   37},
            {255, 75,  0},
            {0,   0,   255},
            {255, 207, 0},
            {255, 0,   226},
            {255, 245, 0},
            {188, 255, 0},
            {0,   255, 18},
            {0,   255, 75},
            {0,   255, 151},
            {255, 56,  0},
            {245, 255, 0}
    };

    cv::cvtColor(rgba, res, cv::COLOR_RGBA2RGB);

    for (size_t i = 0; i < objects.size(); i++) {
        const Instance &obj = objects[i];
        if (obj.prob < 0.15)
            continue;

        const char *class_name = class_names[obj.label];
        printf("instance: %s.\n", class_name);
        const unsigned char *color = colors[i % 81];
        
        // draw mask
        for (int y = 0; y < res.rows; y++) {
            const uchar *mp = obj.mask.ptr(y);
            uchar *p = res.ptr(y);
            for (int x = 0; x < res.cols; x++) {
                if (mp[x] == 255) {
                    p[0] = cv::saturate_cast<uchar>(p[0] * 0.5 + color[0] * 0.5);
                    p[1] = cv::saturate_cast<uchar>(p[1] * 0.5 + color[1] * 0.5);
                    p[2] = cv::saturate_cast<uchar>(p[2] * 0.5 + color[2] * 0.5);
                }
                p += res.step[1];
            }
        }
    }
    printf("----------------------------------\n");
    // cv::imwrite("result.png", res);    // UIImageFromCVMat(res)
}



/*
int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s [imagepath]\n", argv[0]);
        return -1;
    }

    const char *imagepath = argv[1];

    cv::Mat m = cv::imread(imagepath, 1);
    if (m.empty()) {
        fprintf(stderr, "cv::imread %s failed\n", imagepath);
        return -1;
    }

    std::vector <Object> objects;
    detect_solov2(m, objects);

    draw_objects(m, objects);

    return 0;
}
*/
