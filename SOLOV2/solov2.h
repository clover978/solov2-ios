//
//  solov2.h
//  squeenzecnn
//
//  Created by clover on 2021/5/21.
//  Copyright © 2021 dang. All rights reserved.
//

#ifndef solov2_h
#define solov2_h

// Cplusplus STL
#include <stdio.h>
#include <vector>
#include <iostream>
#include <set>
#include <map>

// opencv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ncnn
#include "ncnn/ncnn/net.h"

// iOS UI
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UIKit/UIImage.h>


struct Instance{
    int cx;
    int cy;
    int label;
    float prob;
    cv::Mat mask;
};


class SOLOV2 {
public:
    SOLOV2();
    ~SOLOV2();
    
    void detect_solov2(const cv::Mat &bgr, std::vector<Instance> &objects);
    static void draw_objects(const cv::Mat &bgr, cv::Mat &res, const std::vector <Instance> &objects);

private:
    ncnn::Net* solov2_model;
    int input_w = 768;  // 1536
    int input_h = 432;  // 864
    
    static void nms_sorted_segs(const std::vector <Instance> &objects, std::vector<int> &picked, float nms_threshold, int img_w, int img_h);
    static void ins_decode(const ncnn::Mat &kernel_pred, const ncnn::Mat &feature_pred, ncnn::Mat *ins_pred, int c_in, int c_out, ncnn::Option &opt);
    void generate_res(ncnn::Mat &cate_pred, ncnn::Mat &ins_pred, std::vector <std::vector<Instance>> &objects,
                      float cate_thresh, float conf_thresh, int img_w, int img_h, int num_class, float stride);
};


// helper inline functions: iou, area, sigmoid
inline float intersection_area(const Instance &a, const Instance &b, int img_w, int img_h) {
    float area = 0.f;
    for (int y = 0; y < img_h; y=y+8) {
        for (int x = 0; x < img_w; x=x+8) {
            const uchar *mp1 = a.mask.ptr(y);
            const uchar *mp2 = b.mask.ptr(y);
            if (mp1[x] == 255 && mp2[x] == 255) area += 1.f;
        }
    }
    return area;
}

inline float area(const Instance &a, int img_w, int img_h) {
    float area = 0.f;
    for (int y = 0; y < img_h; y=y+8) {
        for (int x = 0; x < img_w; x=x+8) {
            const uchar *mp = a.mask.ptr(y);
            if (mp[x] == 255) area += 1.f;
        }
    }
    return area;
}

inline float sigmoid(float x) {
    return static_cast<float>(1.f / (1.f + exp(-x)));
}



//static OMP qsort function
static void qsort_descent_inplace(std::vector <Instance> &objects, int left, int right) {
    int i = left;
    int j = right;
    float p = objects[(left + right) / 2].prob;

    while (i <= j) {
        while (objects[i].prob > p)
            i++;

        while (objects[j].prob < p)
            j--;

        if (i <= j) {
            // swap
            std::swap(objects[i], objects[j]);

            i++;
            j--;
        }
    }

    # pragma omp parallel sections
    {
        # pragma omp section
        {
            if (left < j) qsort_descent_inplace(objects, left, j);
        }
        # pragma omp section
        {
            if (i < right) qsort_descent_inplace(objects, i, right);
        }
    }
}

static void qsort_descent_inplace(std::vector <Instance> &objects) {
    if (objects.empty())
        return;
    qsort_descent_inplace(objects, 0, (int)objects.size() - 1);
}


#endif /* solov2_h */
